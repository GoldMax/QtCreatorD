#ifndef BUILDALLBUTTONCONSTANTS_H
#define BUILDALLBUTTONCONSTANTS_H

namespace BuildAllButton {
namespace Constants {

const char ACTION_ID[] = "BuildAllButton.Action";
const char MENU_ID[] = "BuildAllButton.Menu";

} // namespace BuildAllButton
} // namespace Constants

#endif // BUILDALLBUTTONCONSTANTS_H

